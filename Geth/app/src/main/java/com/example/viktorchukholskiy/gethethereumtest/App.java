package com.example.viktorchukholskiy.gethethereumtest;

import android.app.Application;
import android.util.Log;

import org.ethereum.geth.Enode;
import org.ethereum.geth.Enodes;
import org.ethereum.geth.Geth;
import org.ethereum.geth.Node;
import org.ethereum.geth.NodeConfig;
import org.ethereum.geth.NodeInfo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


/**
 * Created by Victor Chuholskiy
 * 28.09.2016.
 */
public class App extends Application {

    private static final String TAG = App.class.getSimpleName();

    private static App sInstance;
    private static Node sNode;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        try {
            connectEthNet();
        } catch (Exception e) {
            sNode = null;
            Log.e(TAG, "Connection node exception: " + e.toString());
        }
    }

    public static synchronized App getInstance() {
        return sInstance;
    }

    public static synchronized Node getNode() {
        return sNode;
    }

    private void connectEthNet() throws Exception {
        //initMainNet();
        //initRopsten();
        initRinkeby();
    }

    private void initMainNet() throws Exception {
        NodeConfig config = new NodeConfig();
        config.setEthereumNetStats("CactusSoft:Respect my authoritah!@stats.rinkeby.io");

        sNode = Geth.newNode(getFilesDir() + "/.ethereum", new NodeConfig());
        sNode.start();

        NodeInfo info = App.getNode().getNodeInfo();
        Log.d(TAG, "My name: " + info.getName());
        Log.d(TAG, "My address: " + info.getListenerAddress());
        Log.d(TAG, "My protocols: " + info.getProtocols());
    }

    private void initRopsten() throws Exception {
        NodeConfig config = new NodeConfig();
        Enode enode1 = new Enode(getString(R.string.enode_ropsten_1));
        Enode enode2 = new Enode(getString(R.string.enode_ropsten_2));
        Enodes enodes = new Enodes();
        enodes.append(enode1);
        enodes.append(enode2);
        config.setBootstrapNodes(enodes);

        config.setEthereumNetworkID(3);
        config.setEthereumGenesis(readGenesis("genesis_ropsten.json"));
        config.setEthereumNetStats("CactusSoft:Respect my authoritah!@stats.ropsten.io");

        sNode = new Node(getFilesDir() + "/.ropsten", config);
        sNode.start();

        NodeInfo info = App.getNode().getNodeInfo();
        Log.d(TAG, "My name: " + info.getName());
        Log.d(TAG, "My address: " + info.getListenerAddress());
        Log.d(TAG, "My protocols: " + info.getProtocols());
    }

    private void initRinkeby() throws Exception {
        Enodes bootnodes = new Enodes();
        bootnodes.append(new Enode("enode://a24ac7c5484ef4ed0c5eb2d36620ba4e4aa13b8c84684e1b4aab0cebea2ae45cb4d375b77eab56516d34bfbd3c1a833fc51296ff084b770b94fb9028c4d25ccf@52.169.42.101:30303?discport=30304"));

        NodeConfig config = new NodeConfig();
        config.setBootstrapNodes(bootnodes);
        config.setEthereumNetworkID(4);
        config.setEthereumGenesis(readGenesis("genesis_rinkeby.json"));
        config.setEthereumNetStats("CactusSoft:Respect my authoritah!@stats.rinkeby.io");

        sNode = new Node(getFilesDir() + "/.rinkeby", config);
        sNode.start();

        NodeInfo info = App.getNode().getNodeInfo();
        Log.d(TAG, "My name: " + info.getName());
        Log.d(TAG, "My address: " + info.getListenerAddress());
        Log.d(TAG, "My protocols: " + info.getProtocols());
    }

    private String readGenesis(String fileName) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(getAssets().open(fileName)));
            StringBuilder stringBuffer = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuffer.append(line);
            }
            return stringBuffer.toString();
        } catch (IOException e) {
            Log.d(TAG, "Read genesis exception: " + e.getMessage());
            return null;
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    Log.d(TAG, "Close stream exception: " + e.getMessage());
                }
            }
        }
    }
}
