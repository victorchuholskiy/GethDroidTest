package com.example.viktorchukholskiy.gethethereumtest;

import org.ethereum.geth.BigInt;


/**
 * Created by viktor.chukholskiy on 22/06/17.
 */

public interface EthGetBalanceListener {
    void ethGetBalanceSuccess(BigInt balance);
}
