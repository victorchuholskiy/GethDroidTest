package com.example.viktorchukholskiy.gethethereumtest;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.view.View;
import android.widget.TextView;

import org.ethereum.geth.BigInt;


public class MainActivity extends AppCompatActivity implements EthGetBalanceListener, View.OnClickListener {

    private TextView tvBalance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tvBalance = (TextView) findViewById(R.id.tv_balance);

        View w1 = findViewById(R.id.wallet_1);
        View w2 = findViewById(R.id.wallet_2);
        View w3 = findViewById(R.id.wallet_3);
        View createAccountButton = findViewById(R.id.create_account);

        w1.setOnClickListener(this);
        w2.setOnClickListener(this);
        w3.setOnClickListener(this);
        createAccountButton.setOnClickListener(this);
    }

    @Override
    public void ethGetBalanceSuccess(BigInt balance) {
        tvBalance.setText(balance.toString());
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.wallet_1:
                // Real net 560,000 Ether
                String address = "0x1b3cb81e51011b549d78bf720b0d924ac763a7c2";
                new EthGetBalanceTask(this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, address);
                break;

            // Rinkeby
            case R.id.wallet_2:
                address = "0x042a7224a7f296226ad8069938ffe3d2be574720";
                new EthGetBalanceTask(this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, address);
                break;
            case R.id.wallet_3:
                address = "0xb5aeaacfc6bb09ca1ba9545a7e58923b35d65382";
                new EthGetBalanceTask(this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, address);
                break;

            case R.id.create_account:
                new EthTestCreateAccountTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;
            default:
                break;
        }
    }
}
