package com.example.viktorchukholskiy.gethethereumtest;

import android.os.AsyncTask;
import android.util.Log;

import org.ethereum.geth.Address;
import org.ethereum.geth.BigInt;
import org.ethereum.geth.EthereumClient;
import org.ethereum.geth.Geth;
import org.ethereum.geth.NodeInfo;

import java.lang.ref.WeakReference;

/**
 * Created by viktor.chukholskiy
 * 19.05.17.
 */

public class EthGetBalanceTask extends AsyncTask<String, Void, BigInt> {

    private static String TAG = EthGetBalanceTask.class.getSimpleName();

    private final WeakReference<EthGetBalanceListener> endListener;

    EthGetBalanceTask(EthGetBalanceListener listener) {
        endListener = new WeakReference<>(listener);
    }

    @Override
    protected BigInt doInBackground(String... params) {
        try {
            return testGetBalance(params[0]);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            return new BigInt(-1);
        }
    }

    @Override
    protected void onPostExecute(BigInt result) {
        super.onPostExecute(result);

        if (endListener.get() != null) {
            endListener.get().ethGetBalanceSuccess(result);
        }
    }

    private BigInt testGetBalance(String addressStr) throws Exception {
        EthereumClient ec = App.getNode().getEthereumClient();
        Log.d(TAG, "Latest block: " + ec.getBlockByNumber(Geth.newContext(), -1).getNumber());

        Address address = new Address(addressStr);
        return ec.getBalanceAt(Geth.newContext(), address, -1);
    }
}
