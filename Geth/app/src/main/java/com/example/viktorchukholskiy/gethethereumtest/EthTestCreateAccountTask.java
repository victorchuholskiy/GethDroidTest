package com.example.viktorchukholskiy.gethethereumtest;

import android.os.AsyncTask;
import android.util.Log;

import org.ethereum.geth.Account;
import org.ethereum.geth.Geth;
import org.ethereum.geth.KeyStore;


/**
 * Created by viktor.chukholskiy
 * 19.05.17.
 */

public class EthTestCreateAccountTask extends AsyncTask<Void, Void, Void> {

    private static String TAG = EthTestCreateAccountTask.class.getSimpleName();

    @Override
    protected Void doInBackground(Void... params) {
        try {

            String destDir = App.getInstance().getFilesDir().getAbsolutePath() + "/.ethereum";
            KeyStore ks = new KeyStore(destDir, Geth.LightScryptN, Geth.LightScryptP);

            Account newAcc = ks.newAccount("123456789Victorcp");
            Log.d(TAG, "New: " + newAcc.getAddress().getHex());
            Log.d(TAG, "Accs: " + ks.getAccounts().size());

            byte[] jsonAcc = ks.exportKey(newAcc, "123456789Victorcp", "123456789Victorep");
            Log.d(TAG, "Json: " + new String(jsonAcc));

            ks.deleteAccount(newAcc, "123456789Victorcp");
            Log.d(TAG, "Accs: " + ks.getAccounts().size());

            Account impAcc = ks.importKey(jsonAcc, "123456789Victorep", "123456789Victorip");
            Log.d(TAG, "Imp: " + impAcc.getAddress().getHex());
            Log.d(TAG, "Accs: " + ks.getAccounts().size());

            ks.deleteAccount(impAcc, "123456789Victorip");
            Log.d(TAG, "Accs: " + ks.getAccounts().size());
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        }
        return null;
    }
}
