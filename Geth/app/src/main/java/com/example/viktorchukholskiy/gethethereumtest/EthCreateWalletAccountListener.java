package com.example.viktorchukholskiy.gethethereumtest;

import android.support.v4.util.Pair;

import org.ethereum.geth.Account;


/**
 * Created by viktor.chukholskiy on 22/06/17.
 */

public interface EthCreateWalletAccountListener {
    void ethCreateWalletAccountSuccess(Pair<Account, Exception> balance);
}
